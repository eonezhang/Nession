
package org.nession.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author wangyj9
 * @version 1.0
 * @since 2013年12月22日 下午7:34:29
 */
public class HelloWorld implements IHelloWorld
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ailk.toptea.sysmgr.common.hessian.service.IHelloWorld#hello(java.lang.String)
	 */
	@Override
	public String hello(String name)
	{
		System.out.println(Thread.currentThread().getName() + ":" + name);
		return "你好" + name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ailk.toptea.sysmgr.common.hessian.service.IHelloWorld#sendBigData(java.util
	 * .Map)
	 */
	@Override
	public boolean sendBigData(Map map)
	{
		System.out.println(map);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ailk.toptea.sysmgr.common.hessian.service.IHelloWorld#reciveBigData(java.lang
	 * .String)
	 */
	@Override
	public Map<String, Object> reciveBigData(String id)
	{
		System.out.println(id);
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		for (int i = 0; i < 100; i++)
		{
			map.put(String.valueOf(i), i);
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ailk.toptea.sysmgr.common.hessian.service.IHelloWorld#upload(java.io.File)
	 */
	@Override
	public boolean upload(File file)
	{
		System.out.println("收到文件：" + file.getAbsolutePath());
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			System.out.println("内容：" + reader.readLine());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return file != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ailk.toptea.sysmgr.common.hessian.service.IHelloWorld#introduce(com.ailk.toptea
	 * .sysmgr.common.hessian.service.Person)
	 */
	@Override
	public String introduce(Person p)
	{
		System.out.println("新来的朋友：" + p);
		return "欢迎你：" + p.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.linkage.toptea.sysmgr.common.hessian.service.IHelloWorld#sendMsg(java.lang.
	 * String)
	 */
	@Override
	public void sendMsg(String msg)
	{
		System.out.println("收到客户端发来的消息：" + msg);
	}
}
