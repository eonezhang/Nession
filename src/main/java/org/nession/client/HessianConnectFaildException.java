
package org.nession.client;

/**
 * Hessian连接失败异常
 * 
 * @author 君枫
 * @version 1.0
 * @since 2013-12-26 下午10:37:31
 */
public class HessianConnectFaildException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6999867662961657788L;

	public HessianConnectFaildException(String error, Throwable cause)
	{
		super(error, cause);
	}
}
